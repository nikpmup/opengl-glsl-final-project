/*
 * Daniel Hall (dhall02)
 * CPE 471
 * Final Project
 * RenderBlur.h
 */

#ifndef _RENDERBLUR_H
#define _RENDERBLUR_H

#include "FBO.h"

class RenderBlur
{
	public:
		static int Install(char *vShader, char *fShader);
		static void UseProg();
		static void DrawBlur(FBO &fbo, int dir);

	private:
		
		RenderBlur();
		static void BindData();
};

#endif
