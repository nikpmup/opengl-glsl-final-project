#ifndef _CMESH_H_
#define _CMESH_H_

#include <string>
#include <vector>
/*
 * Daniel Hall (dhall02)
 * CPE 471
 * Final Project
 * Mesh.h
 */

#ifdef __APPLE__
#include "GLUT/glut.h"
#include <OPENGL/gl.h>
#endif

#ifdef __unix__
#include <GL/glut.h>
#endif

#ifdef _WIN32
#include <GL\glew.h>
#include <GL\glut.h>
#endif

#include "glm/glm.hpp"
#include "CTM.h"

using namespace glm;
using namespace std;

typedef unsigned int uInt;
typedef std::vector<float> vFloat;
typedef std::vector<uInt> vUInt;
typedef std::string String;
typedef std::vector<vec3> vVec3;

enum mData 
{
	mPOS,
	mNORM,
	mUV,
	mIDX,
	mDATA_CNT
};

class CMesh
{
	public:
		vFloat vertices;
		vFloat normal;
		vFloat uv;
		vUInt indices;
		GLuint data[mDATA_CNT];
		CTM transform;

		CMesh() {}
		CMesh(vFloat &pos, vFloat &norm, vUInt &ind);
		CMesh(const String &fileName);
		void InitMesh();
		mat4 GetModel();
		void TranObj(vec3 pos);
		void RotObj(float angle, vec3 axis);
		vec3 GetLoc();

	private:

		mat4 prevCTM;
		bool loadASCIIObj(const String &fileName);
		bool loadASCIIMesh(const String &fileName);
		void calcNormal();
		void addNormal(const vec3 &norm, const uInt &idx);
		vec3 getVertex(const uInt &idx);
		void calcMinMax(vec3 &min, vec3 &max);
		void resizeMesh(const vec3 &size);
		void centerMeshByExtents(const vec3 &CenterLoc);
};

#endif
