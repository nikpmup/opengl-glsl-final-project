/*
 * Daniel Hall (dhall02)
 * CPE 471
 * Final Project
 * Texture.h
 */

#ifndef _TEXTURE_H
#define _TEXTURE_H

#include <GL/glut.h>

class Texture
{
	public:
		GLuint texID;
		Texture() {}
		void InitTexture(const char *fileName, GLuint id, int repeat = 0);
	private:

		typedef struct Image {
			unsigned long sizeX;
			unsigned long sizeY;
			char *data;
		} Image;

		int BMPImageLoad(const char *filename, Image *image);
		GLvoid BMPLoadTexture(const char* image_file, int tex_id, int repeat);
};

#endif
