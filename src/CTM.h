/*
 * Daniel Hall (dhall02)
 * CPE 471
 * Final Project
 * CTM.h
 */

#ifndef _CTM_H
#define _CTM_H

#include "glm/glm.hpp"

using namespace glm;

typedef const vec3 CVEC3;
typedef const float CFLOAT;

class CTM
{
	public:

		CTM();

      CVEC3 GetTrans();

		void SetTranslate(CVEC3 &trans);
		void SetScale(CVEC3 &scale);
		void SetRotate(CFLOAT &angle, CVEC3 &rot);

		void AddTranslate(CVEC3 &translate);
		void AddScale(CVEC3 &scale);
		void AddRotate(CFLOAT &angle, CVEC3 &rot);
		void Reset();
		mat4 GetModelMatrix();

	private:

		vec3 scale;
		vec3 trans;
		mat4 rotate;
		mat4 prevCTM;
};

#endif
