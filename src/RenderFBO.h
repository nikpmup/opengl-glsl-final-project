/*
 * Daniel Hall (dhall02)
 * CPE 471
 * Final Project
 * RenderFBO.h
 */

#ifndef _RENDER_FBO_
#define _RENDER_FBO_

#include "FBO.h"

class RenderFBO
{
	public:
		static int Install(char *vShader, char *fShader);
		static void UseProg();
		static void DrawFBO(FBO &fbo, int width, int height);
		
	private:

		RenderFBO();
};

#endif
