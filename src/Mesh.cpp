/*
 * Daniel Hall (dhall02)
 * CPE 471
 * Final Program
 * Mesh.cpp
 */

#include "Mesh.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <math.h>
#include <iomanip>
#include <string>
#include <streambuf>
#include <algorithm>

#include "glm/glm.hpp"
#include "glm/gtx/transform.hpp"

using namespace glm;

#define VERT_LEN 3
#define X_LOC 0
#define Y_LOC 1
#define Z_LOC 2

// Loads provided mesh into MeshLoader
CMesh::CMesh(vFloat &pos, vFloat &norm, vUInt &ind)
{
	vertices = pos;
	indices = ind;
	normal = norm;
	InitMesh();
}

// Initialises Mesh with an object file
CMesh::CMesh(const String &fileName)
{
	if (fileName.find(".obj"))
	{
		loadASCIIObj(fileName);
	}
	else 
	{
		loadASCIIMesh(fileName);
		centerMeshByExtents(vec3(0, 0, 0));
		resizeMesh(vec3(1, 1, 1));
		calcNormal();
	}
	InitMesh();
}

// Loads a .obj file
bool CMesh::loadASCIIObj(const String &fileName)
{
	std::ifstream File;
	File.open(fileName.c_str());

	if (! File.is_open())
	{
		std::cerr << "Unable to open mesh file: " << fileName << std::endl;
		return false;
	}

	vFloat temp_n, temp_uv, temp_v;
	vUInt temp_nIdx, temp_uIdx, temp_vIdx;

	while (File)
	{
		std::string ReadString;
		std::getline(File, ReadString);

		replace(ReadString.begin(), ReadString.end(), '/', ' ');
		std::stringstream Stream(ReadString);

		std::string Label;
		Stream >> Label;

		if (Label.find("#") != std::string::npos)
		{
			// Comment, skip
			continue;
		}

		if ("v" == Label)
		{
			vec3 Position;
			Stream >> Position.x;
			Stream >> Position.y;
			Stream >> Position.z;

			temp_v.push_back(Position.x);
			temp_v.push_back(Position.y);
			temp_v.push_back(Position.z);
		}
		else if ("vn" == Label)
		{
			vec3 norm;

			Stream >> norm.x;
			Stream >> norm.y;
			Stream >> norm.z;
	
			temp_n.push_back(norm.x);
			temp_n.push_back(norm.y);
			temp_n.push_back(norm.z);
		}
		else if ("vt" == Label)
		{
			vec2 tex;

			Stream >> tex.x;
			Stream >> tex.y;

			temp_uv.push_back(tex.x);
			temp_uv.push_back(tex.y);
		}
		else if ("f" == Label)
		{
			//int Vertex1, Vertex2, Vertex3;
			int vert[3], nIdx[3], uIdx[3];
			Stream >> vert[0];
			Stream >> uIdx[0];
			Stream >> nIdx[0];
			Stream >> vert[1];
			Stream >> uIdx[1];
			Stream >> nIdx[1];
			Stream >> vert[2];
			Stream >> uIdx[2];
			Stream >> nIdx[2];

			temp_vIdx.push_back(vert[0] - 1);
			temp_vIdx.push_back(vert[1] - 1);
			temp_vIdx.push_back(vert[2] - 1);

			temp_uIdx.push_back(uIdx[0] - 1);
			temp_uIdx.push_back(uIdx[1] - 1);
			temp_uIdx.push_back(uIdx[2] - 1);

			temp_nIdx.push_back(nIdx[0] - 1);
			temp_nIdx.push_back(nIdx[1] - 1);
			temp_nIdx.push_back(nIdx[2] - 1);
		}
		else if ("" == Label)
		{
			// Just a new line, carry on...
		}
		else if ("s" == Label)
		{
			// TODO	
		}
		else if ("Corner" == Label)
		{
			// We're not doing any normal calculations... (oops!)
		}
		else
		{
			std::cerr << "While parsing ASCII mesh: Expected 'Vertex' or 'Face' label, found '" << Label << "'." << std::endl;
		}
	}

	int index;
	int uvIndex;
	int nIndex;
	int idxSize = temp_vIdx.size();
	// Orders the normal, UV, and vertice points to have the same index
	for (int i = 0; i < idxSize; i++)
	{
		indices.push_back(i);

		index = 3 * temp_vIdx[i];	
		uvIndex = 2 * temp_uIdx[i];
		nIndex = 3 * temp_nIdx[i];

		vertices.push_back(temp_v[index]);
		vertices.push_back(temp_v[index + 1]);
		vertices.push_back(temp_v[index + 2]);

		uv.push_back(temp_uv[uvIndex]);
		uv.push_back(temp_uv[uvIndex + 1]);

		normal.push_back(temp_n[nIndex]);
		normal.push_back(temp_n[nIndex + 1]);
		normal.push_back(temp_n[nIndex + 2]);
	}
	File.close();
	return true;
}

// Loads the mesh class with the mesh provided
bool CMesh::loadASCIIMesh(const String &fileName)
{
	std::ifstream File;
	File.open(fileName.c_str());

	if (! File.is_open())
	{
		std::cerr << "Unable to open mesh file: " << fileName << std::endl;
		return false;
	}

	while (File)
	{
		std::string ReadString;
		std::getline(File, ReadString);

		std::stringstream Stream(ReadString);

		std::string Label;
		Stream >> Label;

		if (Label.find("#") != std::string::npos)
		{
			// Comment, skip
			continue;
		}

		if ("Vertex" == Label)
		{
			int Index;
			Stream >> Index; // We don't care, throw it away

			vec3 Position;
			Stream >> Position.x;
			Stream >> Position.y;
			Stream >> Position.z;

			vertices.push_back(Position.x);
			vertices.push_back(Position.y);
			vertices.push_back(Position.z);
		}
		else if ("Face" == Label)
		{
			int Index;
			Stream >> Index; // We don't care, throw it away

			int Vertex1, Vertex2, Vertex3;
			Stream >> Vertex1;
			Stream >> Vertex2;
			Stream >> Vertex3;

			indices.push_back(Vertex1 - 1);
			indices.push_back(Vertex2 - 1);
			indices.push_back(Vertex3 - 1);

			size_t Location;
			if ((Location = ReadString.find("{")) != std::string::npos) // there is a color
			{
				Location = ReadString.find("rgb=(");
				Location += 5; // rgb=( is 5 characters

				ReadString = ReadString.substr(Location);
				std::stringstream Stream(ReadString);
				float Color;
				Stream >> Color;
				//Triangle.Color.Red = Color;
				Stream >> Color;
				//Triangle.Color.Green = Color;
				Stream >> Color;
				//Triangle.Color.Blue = Color;
			}

			//Mesh->Triangles.push_back(Triangle);
		}
		else if ("" == Label)
		{
			// Just a new line, carry on...
		}
		else if ("Corner" == Label)
		{
			// We're not doing any normal calculations... (oops!)
		}
		else
		{
			std::cerr << "While parsing ASCII mesh: Expected 'Vertex' or 'Face' label, found '" << Label << "'." << std::endl;
		}
	}

	return true;
}

// Calculates the normals of the vertices
void CMesh::calcNormal()
{
	vec3 pos1, pos2, pos3, v1, v2;
	uInt i1, i2, i3;

	normal.resize(vertices.size() * 3, 0);
	vUInt::iterator iter = indices.begin();
	while (iter != indices.end())
	{
		i1 = *iter++;
		i2 = *iter++;
		i3 = *iter++;

		pos1 = getVertex(i1);
		pos2 = getVertex(i2);
		pos3 = getVertex(i3);

		v1 = pos2 - pos1;
		v2 = pos3 - pos1;

		v1 = cross(v1 , v2);

		addNormal(v1, i1);
		addNormal(v1, i2);
		addNormal(v1, i3);
	}
}

// Helper function to add normals from a vec
void CMesh::addNormal(const vec3 &norm, const uInt &idx)
{
	uInt index = idx * VERT_LEN;

	normal[index++] += norm.x;
	normal[index++] += norm.y;
	normal[index] += norm.z;
}

// Returns the vector of the provided indx
vec3 CMesh::getVertex(const uInt &idx)
{
	vec3 pos;
	uInt index = idx * VERT_LEN;

	pos.x = vertices[index++];
	pos.y = vertices[index++];
	pos.z = vertices[index];

	return pos;
}

// Centers mesh by the provided parameter
void CMesh::centerMeshByExtents(const vec3 &CenterLoc)
{
	vec3 min, max, center, offset;

	calcMinMax(min, max);
	center = (max + min) / vec3(2, 2, 2);
	offset = CenterLoc - center;

	vFloat::iterator iter = vertices.begin();
	while (iter != vertices.end())
	{
		*iter++ += offset.x;
		*iter++ += offset.y;
		*iter++ += offset.z;
	}
}

// Resizes the mesh to the provided size
void CMesh::resizeMesh(const vec3 &size)
{
	vec3 min, max, range, resize;

	calcMinMax(min, max);
	range = max - min;
	float extent = std::max(range.x, std::max(range.y, range.z));
	resize = size / vec3(extent, extent, extent);

	vFloat::iterator iter = vertices.begin();
	while (iter != vertices.end())
	{
		*iter++ *= resize.x;
		*iter++ *= resize.y;
		*iter++ *= resize.z;
	}
}

// Calculates the min and max of the mesh
void CMesh::calcMinMax(vec3 &min, vec3 &max)
{
	vFloat::iterator iter = vertices.begin();
	vec3 cur(*iter++, *iter++, *iter++);

	min = cur;
	max = cur;
	while (iter != vertices.end())
	{
		cur = vec3(*iter++, *iter++, *iter++);

		if (min.x > cur.x)
			min.x = cur.x;
		if (max.x < cur.x)
			max.x = cur.x;

		if (min.y > cur.y)
			min.y = cur.y;
		if (max.y < cur.y)
			max.y = cur.y;

		if (min.z > cur.z)
			min.z = cur.z;
		if (max.z < cur.z)
			max.z = cur.z;
	}

}

// Buffers the mesh into the shader program
void CMesh::InitMesh()
{
	glGenBuffers(1, &data[mPOS]);	
	glBindBuffer(GL_ARRAY_BUFFER, data[mPOS]);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float),
			&vertices.front(), GL_STATIC_DRAW);

	glGenBuffers(1, &data[mNORM]);	
	glBindBuffer(GL_ARRAY_BUFFER, data[mNORM]);
	glBufferData(GL_ARRAY_BUFFER, normal.size() * sizeof(float),
			&normal.front(), GL_STATIC_DRAW);

	glGenBuffers(1, &data[mUV]);
	glBindBuffer(GL_ARRAY_BUFFER, data[mUV]);
	glBufferData(GL_ARRAY_BUFFER, uv.size() * sizeof(float),
			&uv.front(), GL_STATIC_DRAW);

	glGenBuffers(1, &data[mIDX]);	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, data[mIDX]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(float),
			&indices.front(), GL_STATIC_DRAW);
}

// For heirarchy transforms

// Translate in a heirarchy
void CMesh::TranObj(vec3 pos)
{
	prevCTM = translate(pos) * prevCTM;
}

// Rotate in a heirarchy
void CMesh::RotObj(float angle, vec3 axis)
{
	prevCTM = rotate(angle, axis) * prevCTM;
}

// Get the model matrix
mat4 CMesh::GetModel()
{
	return prevCTM * transform.GetModelMatrix();	
}

// Gets the location(position) from the model matrix
vec3 CMesh::GetLoc()
{
	mat4 parent = GetModel();
	return vec3(parent[3][0],
					parent[3][1],
					parent[3][2]);
}
