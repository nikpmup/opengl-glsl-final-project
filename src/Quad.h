/*
 * Daniel Hall (dhall02)
 * CPE 471
 * Final Project
 * Quad.cpp
 */

#ifndef _QUAD_H
#define _QUAD_H

#include <GL/glut.h>
#include "FBO.h"

class Quad
{
	public:
		void BindData(int progID);
		void DrawQuad(int texture);
	private:
		enum eFBO
		{
			VERTEX,
			UV,
			TEXID,
			FBO_CNT
		};

		enum eQuad
		{
			Q_POS,
			Q_IDX,
			Q_UV,
			Q_CNT
		};

		void InitQuad();
		GLuint quadBuff[Q_CNT];
		GLuint gl_fbo[FBO_CNT];
};


#endif
