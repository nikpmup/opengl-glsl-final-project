/*
 * Daniel Hall (dhall02)
 * CPE 471
 * Final Project
 * RenderGlow.cpp
 */

#include "RenderGlow.h"

#include <GL/glut.h>

#include "InstallShader.h"
#include "GLSL_helper.h"
#include "Quad.h"

enum eGlow
{
	eSRC,
	eDST,
	eCNT
};

static Quad quad;
static GLuint gl_glow[eCNT];
static int progID;

// Installs the vert and frag shader to the program
int RenderGlow::Install(char *vShader, char *fShader)
{
	progID = InstallShader::Install(vShader, fShader);	

	glUseProgram(progID);
	RenderGlow::BindData();
	quad.BindData(progID);
	glUseProgram(0);

	return progID;
}

// Binds the gluints to variables in the program
void RenderGlow::BindData()
{
	gl_glow[eSRC] = safe_glGetUniformLocation(progID, "uSrc");	
	gl_glow[eDST] = safe_glGetUniformLocation(progID, "uDst");	
}

// Sets the program render to this render
void RenderGlow::UseProg()
{
	glUseProgram(progID);	
}

// Blends the two fbo's using screen blending
void RenderGlow::DrawGlow(FBO &fbo, FBO &fbo2)
{
	if (!progID)
	{
		fprintf(stderr, "RenderGlow shader not installed yet\n");
		exit(1);
	}
	
	glUseProgram(progID);

	glEnable(GL_TEXTURE_2D);
	glActiveTexture(GL_TEXTURE0 + fbo.color);
	glBindTexture(GL_TEXTURE_2D, fbo.color);
	safe_glUniform1i(gl_glow[eSRC], fbo.color);

	glActiveTexture(GL_TEXTURE0 + fbo2.color);
	glBindTexture(GL_TEXTURE_2D, fbo2.color);
	safe_glUniform1i(gl_glow[eDST], fbo2.color);
	glDisable(GL_TEXTURE_2D);

	quad.DrawQuad(-1);

	glUseProgram(0);
}

