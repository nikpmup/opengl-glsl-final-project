/*
 * Daniel Hall (dhall02)
 * CPE 471
 * Final Project
 * Render.cpp
 */

#include "Render.h"

#include "InstallShader.h"
#include "GLSL_helper.h"
#include "glm/gtc/type_ptr.hpp"

enum shadeAttr 
{
	AMB,
	DIFF,
	SPEC,
	REFL,
	SHADEATTR_CNT
};

enum matrix
{
	MODEL,
	NORM,
	VIEW,
	PROJ,
	MATRIX_CNT
};

enum data
{
	POSITION,
	NORMAL,
	LIGHT,
	LCOLOR,
	CAMERA,
	UV,
	TEX_ID,
	DATA_CNT
};

enum flags
{
	IS_TEX,
	IS_RIM,
	IS_COMB,
	FLAG_CNT	
};

static int progID;
static GLuint gl_flag[FLAG_CNT];
static GLuint gl_data[DATA_CNT];
static GLuint gl_shadeAttr[SHADEATTR_CNT];
static GLuint gl_matrix[MATRIX_CNT];

/* 
 * Installs the provided shader programs to this
 * render and binds the data to it
 */
int Render::Install(char *vShader, char *fShader)
{
	// Installs Shader
	progID = InstallShader::Install(vShader, fShader);	

	// Binds GLSL Data
	glUseProgram(progID);
	Render::BindData();
	glUseProgram(0);

	return progID;
}

// Binds the gluint to variables in the shader prog
void Render::BindData()
{
	gl_data[POSITION] = safe_glGetAttribLocation(progID, "aPosition");
	gl_data[NORMAL] = safe_glGetAttribLocation(progID, "aNormal");
	gl_data[LIGHT] = safe_glGetUniformLocation(progID, "uLightPos");
	gl_data[UV] = safe_glGetAttribLocation(progID, "aTexCoord");
	gl_data[TEX_ID] = safe_glGetUniformLocation(progID, "uTexID");
	gl_data[CAMERA] = safe_glGetUniformLocation(progID, "uCamPos");
	gl_data[LCOLOR] = safe_glGetUniformLocation(progID, "uLightColor");

	gl_matrix[PROJ] = safe_glGetUniformLocation(progID, "uProjMatrix");
	gl_matrix[VIEW] = safe_glGetUniformLocation(progID, "uViewMatrix");
	gl_matrix[MODEL] = safe_glGetUniformLocation(progID, "uModelMatrix");
	gl_matrix[NORM] = safe_glGetUniformLocation(progID, "uNormMatrix");

	gl_shadeAttr[AMB] = safe_glGetUniformLocation(progID, "uAmbColor");
	gl_shadeAttr[DIFF] = safe_glGetUniformLocation(progID, "uDiffColor");
	gl_shadeAttr[SPEC] = safe_glGetUniformLocation(progID, "uSpecColor");
	gl_shadeAttr[REFL] = safe_glGetUniformLocation(progID, "uRefl");

	gl_flag[IS_TEX] = safe_glGetUniformLocation(progID, "uTexFlag");
	gl_flag[IS_RIM] = safe_glGetUniformLocation(progID, "uRim");
	gl_flag[IS_COMB] = safe_glGetUniformLocation(progID, "uComb");
}

// Uses this Renders program
void Render::UseProg()
{
	glUseProgram(progID);
}

// Draws the mesh with only the material
void Render::DrawMat(CMesh &mesh, CMaterial &mat)
{
	glUseProgram(progID);

	Render::SetMat(mat);

	safe_glUniform1i(gl_flag[IS_TEX], 0);
	safe_glUniform1i(gl_flag[IS_RIM], 0);
	safe_glUniform1i(gl_flag[IS_COMB], 0);
	
	Render::DrawMesh(mesh);
}

// Draws the mesh with the texture and the rim effect
void Render::DrawRimTex(CMesh &mesh, GLuint texID)
{
	glUseProgram(progID);

	glEnable(GL_TEXTURE_2D);

	Render::BindTex(texID);
	safe_glUniform1i(gl_flag[IS_RIM], 1);
	safe_glUniform1i(gl_flag[IS_COMB], 0);

	Render::DrawMesh(mesh);	

	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);
}

// Draws the mesh with the texture
void Render::DrawTex(CMesh &mesh, GLuint texID)
{
	glUseProgram(progID);

	glEnable(GL_TEXTURE_2D);		

	Render::BindTex(texID);	
	safe_glUniform1i(gl_flag[IS_RIM], 0);
	safe_glUniform1i(gl_flag[IS_COMB], 0);

	Render::DrawMesh(mesh);

	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);
}

// Draws the mesh with the texture and the material properties
void Render::DrawMatTex(CMesh &mesh, CMaterial &mat, GLuint texID)
{
	glUseProgram(progID);

	safe_glUniform1i(gl_flag[IS_RIM], 0);
	safe_glUniform1i(gl_flag[IS_COMB], 1);

	Render::SetMat(mat);
	Render::BindTex(texID);
	Render::DrawMesh(mesh);

	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);
}

// Binds the texture to the shader
void Render::BindTex(GLuint texID)
{
	glActiveTexture(GL_TEXTURE0 + texID);
	glBindTexture(GL_TEXTURE_2D, texID);

	safe_glUniform1i(gl_data[TEX_ID], texID);
	safe_glUniform1i(gl_flag[IS_TEX], 1);
}

// Draws the mesh
void Render::DrawMesh(CMesh &mesh)
{
	Render::SetModel(mesh.GetModel());

	safe_glEnableVertexAttribArray(gl_data[POSITION]);
	glBindBuffer(GL_ARRAY_BUFFER, mesh.data[mPOS]);
	safe_glVertexAttribPointer(gl_data[POSITION], 3, GL_FLOAT,
			GL_FALSE, 0, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh.data[mIDX]);

	safe_glEnableVertexAttribArray(gl_data[NORMAL]);
	glBindBuffer(GL_ARRAY_BUFFER, mesh.data[mNORM]);
	safe_glVertexAttribPointer(gl_data[NORMAL], 3, GL_FLOAT,
			GL_FALSE, 0, 0);

	safe_glEnableVertexAttribArray(gl_data[UV]);
	glBindBuffer(GL_ARRAY_BUFFER, mesh.data[mUV]);
	safe_glVertexAttribPointer(gl_data[UV], 2, GL_FLOAT, GL_FALSE, 0, 0);

	glDrawElements(GL_TRIANGLES, mesh.indices.size(), GL_UNSIGNED_INT, 0);
	
	safe_glDisableVertexAttribArray(gl_data[POSITION]);
	safe_glDisableVertexAttribArray(gl_data[NORMAL]);
	safe_glDisableVertexAttribArray(gl_data[UV]);
}

// Sets the projection matrix
void Render::SetProjection(const mat4 &proj)
{
	safe_glUniformMatrix4fv(gl_matrix[PROJ], glm::value_ptr(proj));
}

// Sets the view matrix
void Render::SetView(const mat4 &view)
{
	safe_glUniformMatrix4fv(gl_matrix[VIEW], glm::value_ptr(view));
}

// Sets the model and the normal matrix
void Render::SetModel(const mat4 &model)
{
	safe_glUniformMatrix4fv(gl_matrix[NORM], glm::value_ptr(transpose(inverse(model))));
	safe_glUniformMatrix4fv(gl_matrix[MODEL], glm::value_ptr(model));
}

// Sets the light position
void Render::SetLight(const vec3 &pos)
{
	safe_glUniform3fv(gl_data[LIGHT], glm::value_ptr(pos));
}

// Sets the camera location (used for rim effect)
void Render::SetCam(const vec3 &pos)
{
	safe_glUniform3fv(gl_data[CAMERA], glm::value_ptr(pos));
}

// Returns the programs ID
int Render::GetProgID()
{
	return progID;
}

// Sets the material of the render
void Render::SetMat(CMaterial &mat)
{
	safe_glUniform3fv(gl_shadeAttr[AMB], glm::value_ptr(mat.getAmb()));
	safe_glUniform3fv(gl_shadeAttr[DIFF], glm::value_ptr(mat.getDiff()));
	safe_glUniform3fv(gl_shadeAttr[SPEC], glm::value_ptr(mat.getSpec()));
	safe_glUniform1f(gl_shadeAttr[REFL], mat.getRefl());
}

// Sets the light color of the render
void Render::SetLightColor(const vec3 &col)
{
	safe_glUniform3fv(gl_data[LCOLOR], glm::value_ptr(col));	
}
