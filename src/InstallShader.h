/*
 * Daniel Hall (dhall02)
 * CPE 471
 * Final Project
 * InstallShader.h
 */

#ifndef _INSTALLSHADER_H
#define _INSTALLSHADER_H

class InstallShader
{
	public:
		static int Install(char *vShader, char *fShader);
};

#endif
