/*
 * Daniel Hall (dhall02)
 * CPE 471
 * Final Program
 * MAterial.h
 */

#ifndef _MATERIAL_H
#define _MATERIAL_H

#include "glm/glm.hpp"

using namespace glm;

typedef const vec3 CVEC3;
typedef const float CFLOAT;

class CMaterial
{
	public:
		
		CMaterial();
		CMaterial(CVEC3 &amb, CVEC3 &diff, CVEC3 &spec, CFLOAT &refl);

		void setAmb(CVEC3 &amb);
		void setDiff(CVEC3 &diff);
		void setSpec(CVEC3 &spec);
		void setRefl(CFLOAT &refl);

		CVEC3& getAmb();
		CVEC3& getDiff();
		CVEC3& getSpec();
		CFLOAT& getRefl();

	private:

		vec3 ambCoef;
		vec3 diffCoef;
		vec3 specCoef;
		float reflect;
};

#endif
