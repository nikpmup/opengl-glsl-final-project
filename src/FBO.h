#ifndef _FBO_H
#define _FBO_H

#include <GL/glut.h>


class FBO
{
	public:
		GLuint id;
		GLuint color;
		GLuint depth;
		int width, height;

		FBO(){}
		FBO(int texNum, int width, int height);
		void Start();
		void End();

	private:

};

#endif
