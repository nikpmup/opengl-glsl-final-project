/*
 * Daniel Hall (dhall02)
 * CPE 471
 * Final Proj
 * Camera.h
 */

#ifndef _CAMERA_H_
#define _CAMERA_H_

#include "glm/glm.hpp"
#include "CTM.h"

typedef const float CFLOAT;
typedef const vec3 CVEC3;
typedef const int CINT;

class CCamera
{
	public:
		
		CCamera();
		// Get Transform Matrix
		mat4 getProjMatrix();
		mat4 getViewMatrix();
		vec3 getPosition();
		
		// Projection setup
		void setFOV(CFLOAT &fov);
		void setRatio(CFLOAT &ratio);
		void setScreen(int *width, int *height);	
		void setZNear(CFLOAT &z);
		void setZFar(CFLOAT &z);
		void setProj(CFLOAT &fov, CFLOAT &ratio, CFLOAT &zNear, CFLOAT &zFar);

		// View Setup
		void setPos(CVEC3 &pos);
		void setLook(CVEC3 &look);
		void setUp(CVEC3 &up);
		void setView(CVEC3 &pos, CVEC3 &look, CVEC3 &up);
		void calcLook(CFLOAT phi, CFLOAT theta);
		void moveForward(CFLOAT &amt);
		void moveBack(CFLOAT &amt);
		void strafeLeft(CFLOAT &amt);
		void strafeRight(CFLOAT &amt);
		void setHeight(CFLOAT &height);

		void Reset();

		vec3 pitchYaw;
	private:

		float fov, ratio, zNear, zFar;
		int *width, *height;
		vec3 pos, look, up;
		vec3 camX, camY, camZ;
		vec3 camHeight, camMove;

		void clamp(float &angle, CFLOAT &center, CFLOAT &rad);
		void calcCamCoord();
};

#endif
