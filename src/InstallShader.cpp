/*
 * Daniel Hall (dhall02)
 * CPE 471
 * Final Project
 * InstallShader.cpp
 */

#include <GL/glut.h>
#include "GLSL_helper.h"

#include "InstallShader.h"

// Installs a shader program
int InstallShader::Install(char *vShader, char *fShader)
{
	const GLchar *vProg = textFileRead(vShader);	
	const GLchar *fProg = textFileRead(fShader);
	GLuint VS;
	GLuint FS;
	int ShadeProg;

	GLint vCompiled, fCompiled, linked; //status of shader
	
	VS = glCreateShader(GL_VERTEX_SHADER);
	FS = glCreateShader(GL_FRAGMENT_SHADER);
	
	//load the source
	glShaderSource(VS, 1, &vProg, NULL);
	glShaderSource(FS, 1, &fProg, NULL);
	
	//compile shader and print log
	glCompileShader(VS);
	printOpenGLError();
	/* check shader status requires helper functions */
	glGetShaderiv(VS, GL_COMPILE_STATUS, &vCompiled);
	printShaderInfoLog(VS);
	
	//compile shader and print log
	glCompileShader(FS);
	/* check shader status requires helper functions */
	printOpenGLError();
	glGetShaderiv(FS, GL_COMPILE_STATUS, &fCompiled);
	printShaderInfoLog(FS);

	if (!fCompiled || !vCompiled) {
		printf("Error compiling the shader %s or %s", vProg, fProg);
		return 0;
	}
	 
	//create a program object and attach the compiled shader
	ShadeProg = glCreateProgram();
	glAttachShader(ShadeProg, VS);
	glAttachShader(ShadeProg, FS);
	
	glLinkProgram(ShadeProg);
	/* check shader status requires helper functions */
	printOpenGLError();
	glGetProgramiv(ShadeProg, GL_LINK_STATUS, &linked);
	printProgramInfoLog(ShadeProg);

	printf("sucessfully installed shader %d\n", ShadeProg);

	return ShadeProg;
}
