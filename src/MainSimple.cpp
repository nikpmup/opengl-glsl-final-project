/*
 * Daniel Hall (dhall02)
 * CPE 471
 * Final Project
 * MainSimple.cpp
 */

#include <iostream>

#ifdef __APPLE__
#include "GLUT/glut.h"
#include <OPENGL/gl.h>
#endif

#ifdef __unix__
#include <GL/glut.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string>
#include "GLSL_helper.h"
#include "glm/glm.hpp"
#include "Mesh.h"
#include "Material.h"
#include "Camera.h"
#include "RenderFBO.h"
#include "RenderBlur.h"
#include "RenderGlow.h"
#include "Render.h"
#include "Texture.h"

using namespace std;

#define SENSITIVITY 3.0
#define FBO_SIZE 1024
#define BLUR_SIZE 512
#define RAND_SIZE 100
#define MOVE 0.1

// Texture Loc
static int texNum = 0;

// Window
static int g_width, g_height;
static float g_gndHeight= -1;

// Navi location
vec3 g_naviPos = vec3(0.0);
vec3 g_naviOrig = vec3(0, 2, 20);
// Light color
vec3 g_lightColor = vec3(0.639215686274, 0.890196078431, 0.929411764705);

enum objects
{
	eNavi,
	eRock,
	eSkyBlock,
	eGnd,
	eDead,
	eStone,
	eCnt
};

// Object files
CMesh mesh[eCnt];
Texture tex[eCnt];
CMaterial mat[eCnt];

typedef enum modes
{
	PLAIN,
	RIM,
	BLUR_P,
	BLUR_F,
	GLOW,
	FINAL,
	MODE_CNT
} modes;

static modes mode = PLAIN;
static bool motion;

// Camera
CCamera camera;

// FBO for blur effect
static FBO normal;
static FBO temp;
static FBO blur;
static FBO glow;

void InitStone()
{
	mesh[eStone] = CMesh("Models/stone.obj");
	mesh[eStone].transform.SetScale(vec3(0.1, 0.1, 0.1));
	mesh[eStone].transform.SetTranslate(vec3(0, g_gndHeight, 5));
	tex[eStone].InitTexture("Texture/stone.bmp", texNum++, true);
	mat[eStone] = CMaterial(
			vec3(0.0, 0.0, 0.0),
			vec3(0.698039, 0.698039, 0.698039),
			vec3(0.898039, 0.898039, 0.898039),
			20);
}

void InitRock()
{
	mesh[eRock] = CMesh("Models/rock.obj");
	tex[eRock].InitTexture("Texture/rock.bmp", texNum++);

	mat[eRock] = CMaterial(
			vec3(0.0, 0.0, 0.0),
			vec3(0.698039, 0.698039, 0.698039),
			vec3(0.898039, 0.898039, 0.898039),
			20);
}

void InitSkyBox()
{
	mesh[eSkyBlock] = CMesh("Models/skybox.obj");
	mesh[eSkyBlock].transform.SetScale(vec3(250, 250, 250));
	mesh[eSkyBlock].transform.SetTranslate(vec3(0, 25, 0));
	
	tex[eSkyBlock].InitTexture("Texture/skybox.bmp",
	 texNum++);
}

void InitNavi()
{
	mesh[eNavi] = CMesh("Models/navi5Div.obj");
	mesh[eNavi].TranObj(g_naviOrig);
	
	tex[eNavi].InitTexture("Texture/navi64.bmp", texNum++);
	
}

void InitGround()
{
	mesh[eGnd] = CMesh("Models/ground.obj");
	mesh[eGnd].transform.SetScale(vec3(250, 1, 250));
	mesh[eGnd].transform.SetTranslate(vec3(0, g_gndHeight, 0));
	mat[eGnd] = CMaterial(
			vec3(0.1, 0.1, 0.1),
			vec3(0.0941, 0.15294, 0.28235),
			vec3(0, 0, 0),
			0.01);
	tex[eGnd].InitTexture("Texture/sand.bmp", texNum++, true);
}

void InitRedead()
{
	mesh[eDead] = CMesh("Models/redead.obj");
	mesh[eDead].transform.SetScale(vec3(0.1, 0.1, 0.1));
	mesh[eDead].transform.SetTranslate(vec3(0, g_gndHeight, 0));
	mat[eDead] = CMaterial(
			vec3(0.0, 0.0, 0.0),
			vec3(0.0941, 0.15294, 0.28235),
			vec3(0, 0, 0),
			0.01);
	tex[eDead].InitTexture("Texture/redead_grp.bmp", texNum++, true);
}

// Initializes all the objects
void InitGeom() 
{
	Render::UseProg();
	InitRock();
	InitSkyBox();
	InitGround();
	InitNavi();
	InitRedead();
	InitStone();
}

void InitCam()
{
	// Sets camera settings
	camera.setScreen(&g_width, &g_height);
	camera.setView(vec3(0, 0, 0), vec3(0, 0, -1), vec3(0, 1, 0));
	camera.setZFar(500.0);
	camera.setHeight(2);
}

void InitFBO()
{
	normal = FBO(texNum++, FBO_SIZE, FBO_SIZE);
	temp = FBO(texNum++, BLUR_SIZE, BLUR_SIZE);
	blur = FBO(texNum++, BLUR_SIZE, BLUR_SIZE);
	glow = FBO(texNum++, FBO_SIZE, FBO_SIZE);
}

void Initialize ()					
{
	glClearDepth (1.0f);	// Depth Buffer Setup
	glDepthFunc (GL_LEQUAL);	// The Type Of Depth Testing
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); // Type of blending
	glClearColor (0.0941, 0.15294, 0.28235, 0); // Background color	
	camera.moveBack(30);

	InitCam();
	InitFBO();
}

// Sets up perspective matricies and light position
void SetupScene()
{
	Render::SetProjection(camera.getProjMatrix());
	Render::SetView(camera.getViewMatrix());
	Render::SetLight(mesh[eNavi].GetLoc());
	Render::SetCam(camera.getPosition());
	Render::SetLightColor(g_lightColor);
}

void DrawPlain()
{
	glEnable(GL_DEPTH_TEST);
	
	if (mode == PLAIN) // Shows plain texture mapping
		Render::DrawTex(mesh[eNavi], tex[eNavi].texID);
	else if (mode == RIM) // Shows rim effect
		Render::DrawRimTex(mesh[eNavi], tex[eNavi].texID);
	else if (mode >= BLUR_P) // Shows blur process
	{
		// Draw Navi to blur fbo
		blur.Start();
			mesh[eNavi].transform.SetScale(vec3(1.12, 1.12, 1.12));
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			Render::DrawTex(mesh[eNavi], tex[eNavi].texID);
		blur.End();
		mesh[eNavi].transform.SetScale(vec3(1, 1, 1));

		if (mode >= BLUR_F)
		{
			// Horizontal blur Navi to temp 
			glEnable(GL_BLEND);
			temp.Start();
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				RenderBlur::DrawBlur(blur, 0);
			temp.End();

			// Vertical blur to blur FBO
			blur.Start();
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				RenderBlur::DrawBlur(temp, 1);
			blur.End();
		}

		// Just shows blur
		if (mode != GLOW)
			RenderFBO::DrawFBO(blur, g_width, g_height);
		else
		{
			//Shows blur blended with normal drawing
			normal.Start();
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				Render::DrawRimTex(mesh[eNavi], tex[eNavi].texID);
			normal.End();

			glow.Start();
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				RenderGlow::DrawGlow(normal, blur); 
			glow.End(); 
			// Draw combined scene
			RenderFBO::DrawFBO(glow, g_width, g_height);
		}

	}
}

void DrawRocks()
{
	// Rock behind redead
	mesh[eRock].transform.SetTranslate(vec3(0, 0, -27));
	mesh[eRock].transform.SetScale(vec3(20, 20, 13));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);

	// "Mountains in front 
	mesh[eRock].transform.SetTranslate(vec3(-150, 0, -200));
	mesh[eRock].transform.SetScale(vec3(250, 46, 133));
	mesh[eRock].transform.SetRotate(90, vec3(0, 1, 0));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);

	mesh[eRock].transform.SetTranslate(vec3(-50, 0, -200));
	mesh[eRock].transform.SetScale(vec3(250, 93, 133));
	mesh[eRock].transform.SetRotate(315, vec3(0, 1, 0));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);

	mesh[eRock].transform.SetTranslate(vec3(-50, 0, -200));
	mesh[eRock].transform.SetScale(vec3(130, 63, 255));
	mesh[eRock].transform.SetRotate(215, vec3(0, 1, 0));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);

	mesh[eRock].transform.SetTranslate(vec3(150, 0, -200));
	mesh[eRock].transform.SetScale(vec3(250, 46, 133));
	mesh[eRock].transform.SetRotate(90, vec3(0, 1, 0));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);

	// Draw Mountains to the right
	mesh[eRock].transform.SetTranslate(vec3(150, 0, -200));
	mesh[eRock].transform.SetScale(vec3(135, 150, 90));
	mesh[eRock].transform.SetRotate(15, vec3(0, 1, 0));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);

	mesh[eRock].transform.SetTranslate(vec3(200, 0, -90));
	mesh[eRock].transform.SetScale(vec3(32, 110, 160));
	mesh[eRock].transform.SetRotate(180, vec3(0, 1, 0));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);

	mesh[eRock].transform.SetTranslate(vec3(200, 0, -20));
	mesh[eRock].transform.SetScale(vec3(62, 32, 100));
	mesh[eRock].transform.SetRotate(90, vec3(0, 1, 0));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);

	mesh[eRock].transform.SetTranslate(vec3(150, 0, 200));
	mesh[eRock].transform.SetScale(vec3(135, 150, 90));
	mesh[eRock].transform.SetRotate(15, vec3(0, 1, 0));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);

	mesh[eRock].transform.SetTranslate(vec3(190, 0, 100));
	mesh[eRock].transform.SetScale(vec3(85, 73, 170));
	mesh[eRock].transform.SetRotate(0, vec3(0, 1, 0));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);

	// Draw Mountains behind
	mesh[eRock].transform.SetTranslate(vec3(-150, 0, 200));
	mesh[eRock].transform.SetScale(vec3(250, 46, 133));
	mesh[eRock].transform.SetRotate(0, vec3(0, 1, 0));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);

	mesh[eRock].transform.SetTranslate(vec3(-50, 0, 250));
	mesh[eRock].transform.SetScale(vec3(250, 93, 133));
	mesh[eRock].transform.SetRotate(165, vec3(0, 1, 0));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);

	mesh[eRock].transform.SetTranslate(vec3(-50, 0, 230));
	mesh[eRock].transform.SetScale(vec3(130, 63, 255));
	mesh[eRock].transform.SetRotate(45, vec3(0, 1, 0));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);

	mesh[eRock].transform.SetTranslate(vec3(150, 0, 210));
	mesh[eRock].transform.SetScale(vec3(250, 46, 133));
	mesh[eRock].transform.SetRotate(170, vec3(0, 1, 0));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);

	// Draw Mountains to the right
	mesh[eRock].transform.SetTranslate(vec3(-150, 0, -200));
	mesh[eRock].transform.SetScale(vec3(135, 150, 90));
	mesh[eRock].transform.SetRotate(15, vec3(0, 1, 0));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);

	mesh[eRock].transform.SetTranslate(vec3(-200, 0, -90));
	mesh[eRock].transform.SetScale(vec3(32, 110, 160));
	mesh[eRock].transform.SetRotate(180, vec3(0, 1, 0));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);

	mesh[eRock].transform.SetTranslate(vec3(-200, 0, -20));
	mesh[eRock].transform.SetScale(vec3(62, 32, 100));
	mesh[eRock].transform.SetRotate(90, vec3(0, 1, 0));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);

	mesh[eRock].transform.SetTranslate(vec3(-150, 0, 200));
	mesh[eRock].transform.SetScale(vec3(135, 150, 90));
	mesh[eRock].transform.SetRotate(15, vec3(0, 1, 0));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);

	mesh[eRock].transform.SetTranslate(vec3(-190, 0, 100));
	mesh[eRock].transform.SetScale(vec3(85, 73, 170));
	mesh[eRock].transform.SetRotate(0, vec3(0, 1, 0));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);

	//Random rocks about
	mesh[eRock].transform.SetTranslate(vec3(75, 0, -27));
	mesh[eRock].transform.SetScale(vec3(13, 5, 7));
	mesh[eRock].transform.SetRotate(35, vec3(0, 1, 0));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);

	mesh[eRock].transform.SetTranslate(vec3(106, 0, -64));
	mesh[eRock].transform.SetScale(vec3(13, 46, 7));
	mesh[eRock].transform.SetRotate(35, vec3(0, 1, 0));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);

	mesh[eRock].transform.SetTranslate(vec3(106, 0, 104));
	mesh[eRock].transform.SetScale(vec3(56, 26, 23));
	mesh[eRock].transform.SetRotate(35, vec3(0, 1, 0));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);

	mesh[eRock].transform.SetTranslate(vec3(-86, 0, 104));
	mesh[eRock].transform.SetScale(vec3(5, 26, 13));
	mesh[eRock].transform.SetRotate(180, vec3(0, 1, 0));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);

	mesh[eRock].transform.SetTranslate(vec3(-100, 0, 104));
	mesh[eRock].transform.SetScale(vec3(13, 13, 4));
	mesh[eRock].transform.SetRotate(180, vec3(0, 1, 0));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);

	mesh[eRock].transform.SetTranslate(vec3(-100, 0, 0));
	mesh[eRock].transform.SetScale(vec3(13, 13, 13));
	mesh[eRock].transform.SetRotate(57, vec3(0, 1, 0));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);

	mesh[eRock].transform.SetTranslate(vec3(-140, 0, -34));
	mesh[eRock].transform.SetScale(vec3(3, 5, 13));
	mesh[eRock].transform.SetRotate(57, vec3(0, 1, 0));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);

	mesh[eRock].transform.SetTranslate(vec3(-90, 0, -64));
	mesh[eRock].transform.SetScale(vec3(13, 45, 19));
	mesh[eRock].transform.SetRotate(57, vec3(0, 1, 0));
	Render::DrawMatTex(mesh[eRock], mat[eRock], tex[eRock].texID);
}

void DrawFinal()
{
	// Draws Scene to Normal FBO
	glEnable(GL_DEPTH_TEST);
	normal.Start();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		DrawRocks();
		Render::DrawMatTex(mesh[eDead], mat[eDead], tex[eDead].texID);
		Render::DrawMatTex(mesh[eStone], mat[eStone], tex[eStone].texID);
		Render::DrawTex(mesh[eSkyBlock], tex[eSkyBlock].texID);
		Render::DrawMatTex(mesh[eGnd], mat[eGnd], tex[eGnd].texID);
		Render::DrawRimTex(mesh[eNavi], tex[eNavi].texID);
	normal.End();

	mesh[eNavi].transform.SetScale(vec3(1.12, 1.12, 1.12));

	// Draw Navi to blur fbo
	blur.Start();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		Render::DrawTex(mesh[eNavi], tex[eNavi].texID);
	blur.End();
	mesh[eNavi].transform.SetScale(vec3(1, 1, 1));
	glDisable(GL_DEPTH_TEST);

	// Horizontal blur Navi to temp 
	glEnable(GL_BLEND);
	temp.Start();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		RenderBlur::DrawBlur(blur, 0);
	temp.End();

	// Vertical blur to blur FBO
	blur.Start();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		RenderBlur::DrawBlur(temp, 1);
	blur.End();

	// Blend normal render scene with glow scene
	glow.Start();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		RenderGlow::DrawGlow(normal, blur); 
	glow.End(); 
	glDisable(GL_BLEND); 
	// Draw combined scene
	RenderFBO::DrawFBO(glow, g_width, g_height);

}

/* Main display function */
void Draw (void)
{
	Render::UseProg();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	SetupScene();

	if (mode != FINAL)
		DrawPlain();
	else
		DrawFinal();
	
	glUseProgram(0);	
	glutSwapBuffers();
}

/* Reshape */
void ReshapeGL (int width, int height)
{
	g_width = (float)width;
	g_height = (float)height;
	glViewport (0, 0, (GLsizei)(width), (GLsizei)(height));
}

//the keyboard callback to change the values to the transforms
void keyboard(unsigned char key, int x, int y )
{
	static const int AMT = 3;
	switch( key ) 
	{
		/* WASD keyes effect view/camera transform */
		case 'w':
			camera.moveForward(AMT);
			break;
		case 's':
			camera.moveBack(AMT);
			break;
		case 'a':
			camera.strafeLeft(AMT);
			break;
		case 'd':
			camera.strafeRight(AMT);
			break;
		case 'm':
			motion = !motion;
			break;
		case 'f':
			mode = FINAL;
			break;
		case 'r':
			mode = PLAIN;
			break;
		case 'p':
			mode = mode == FINAL ? FINAL : (modes)((int)mode + 1);
			break;
		case 'q': case 'Q' :
			exit( EXIT_SUCCESS );
			break;
	}

	glutPostRedisplay();
}

// Handles mouse movement
void mouseMove(int x, int y)
{
	static float xPrev = -1;
	static float yPrev = -1;

	// Initializes the x/y positions
	if (xPrev == -1 && yPrev == -1)
	{
		xPrev = x;
		yPrev = -y;
	}
	else
	{
		float xDiff = x - xPrev;
		float yDiff = -y - xPrev;

		const int offset = 50;
		// warps mouse to center if the mouse reaches near the end of the screen
		if (y >= g_height - offset || y <= 0 + offset || x >= g_width - offset
				|| x <= 0 + offset)
		{
			const float xMid = g_width / 2.0;
			const float yMid = g_height / 2.0;

			xPrev = xMid;
			yPrev = -yMid;
			glutWarpPointer(xMid, yMid);
		}
		// Calculates the angle from the mouse position
		else if (xDiff != 0 || yDiff != 0)
		{
			float phi = 3.14 * (-y - yPrev)/g_height;
			float theta = 3.14 * (x -xPrev)/g_width;

			yPrev = -y;
			xPrev = x;

			camera.calcLook(phi, theta);

			glutPostRedisplay();
		}
	}
}

void moveNavi(int value)
{
	static vec3 vel(0, MOVE / 10.0, 0);
	static int sign = 1;

	if (motion)
	{
		// Handles up/down fall
		if (g_naviPos.y < 1)
		{
			sign = 1;
			vel.y = MOVE / 10.0;
		}
		else if (g_naviPos.y > 3)
		{
			sign = -1;
			vel.y = -MOVE / 10.0;
		}

		vel.y += sign * rand() % 10 / 1000.0;

		g_naviPos += vel;
		//Rotates Navi in a circle
		mesh[eNavi].RotObj(0.5, vec3(0, 1, 0));
		//Applies vertical movement
		mesh[eNavi].transform.SetTranslate(g_naviPos);
	}

	glutPostRedisplay();
	glutTimerFunc(1, moveNavi, 0);
}

int main( int argc, char *argv[] )
{
	g_width = g_height = 600;
	glutInit( &argc, argv );
	glutInitWindowPosition( 200, 200 );
	glutInitWindowSize( g_width, g_height);
	glutInitDisplayMode( GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH );
	glutCreateWindow("My first mesh");
	glutReshapeFunc( ReshapeGL );
	glutDisplayFunc( Draw );
	glutFullScreen();
	glutKeyboardFunc( keyboard );
	glutSetCursor(GLUT_CURSOR_NONE);
	glutPassiveMotionFunc( mouseMove );


	//test the openGL version
	getGLversion();
	//install the shader
	{
		int checkNorm = Render::Install((char*)"shaders/shader_vert.glsl",
				(char*)"shaders/shader_frag.glsl");
		int checkFBO =	RenderFBO::Install((char*)"shaders/fbo_vert.glsl", 
				(char*)"shaders/fbo_frag.glsl");
		int checkBlur = RenderBlur::Install((char*)"shaders/blur_vert.glsl",
				(char*)"shaders/blur_frag.glsl");
		int checkGlow = RenderGlow::Install((char*)"shaders/fbo_vert.glsl",
				(char*)"shaders/glow_frag.glsl");

		if (!checkFBO || !checkBlur || !checkGlow || !checkNorm)
		{
			printf("Error installing shader!\n");
			return 0;
		}
	}
	InitGeom();
	Initialize();

	glutTimerFunc(1, moveNavi, 0);
	glutMainLoop();
	return 0;
}
