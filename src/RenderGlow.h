/*
 * Daniel Hall (dhall02)
 * CPE 471
 * Final Project
 * RenderGlow.h
 */

#ifndef _RENDERGLOW_H
#define _RENDERGLOW_H

#include "FBO.h"

class RenderGlow
{
	public:
		static int Install(char *vShader, char *fShader);	
		static void UseProg();
		static void DrawGlow(FBO &fbo, FBO &fbo2);

	private:
		RenderGlow();
		static void BindData();
};

#endif
