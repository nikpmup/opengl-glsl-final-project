/*
 * Daniel Hall (dhall02)
 * CPE 471
 * Final Project
 * CTM.cpp
 */

#include "CTM.h"
#include "glm/gtx/transform.hpp"
#include <cmath>

// Initializes member variables with no transformations
CTM::CTM() 
{
	Reset();
}

// Resets the transformations
void CTM::Reset()
{
	trans = vec3(0,0,0);
	scale = vec3(1,1,1);
	rotate = glm::mat4(1.0f);
}

CVEC3 CTM::GetTrans()
{
	return trans;
}

// Sets the CTM translation to the provided parameter
void CTM::SetTranslate(CVEC3 &_trans)
{
	trans = _trans;
}

// Sets the CTM scale to the provided parameter
void CTM::SetScale(CVEC3 &_scale)
{
	scale = _scale;
}

// Sets the CTM rotate to the provided parameter
void CTM::SetRotate(CFLOAT &angle, CVEC3 &axis)
{
	rotate = glm::rotate(mat4(1.0f), angle, axis);
}

// Adds translation to current translation
void CTM::AddTranslate(CVEC3 &translate)
{
	trans += translate;
}

// Increases/Decreases scale
void CTM::AddScale(CVEC3 &scale)
{
	this->scale *= scale;
}

// Adds rotation
void CTM::AddRotate(CFLOAT &angle, CVEC3 &axis)
{
	rotate = glm::rotate(angle, axis) * rotate;
}

// Returns the transformation matrix
glm::mat4 CTM::GetModelMatrix()
{
	glm::mat4 scaleMat = glm::scale(glm::vec3(scale));
	glm::mat4 transMat = glm::translate(trans);

	return transMat * scaleMat * rotate;
}
