/*
 * Daniel Hall (dhall02)
 * CPE 471
 * Final Project
 * RenderFBO.cpp
 */

#include "RenderFBO.h"

#include <GL/glut.h>

#include "GLSL_helper.h"
#include "InstallShader.h"
#include "Quad.h"

static Quad quad;
static int progID;


// Installs the frag and vert shader to this render
int RenderFBO::Install(char *vShader, char *fShader)
{
	//Installs Shader
	progID = InstallShader::Install(vShader, fShader);

	// Binds GLSL Data
	glUseProgram(progID);
	quad.BindData(progID);

	glUseProgram(0);

	return progID;
}

// Causes the program to use this render
void RenderFBO::UseProg()
{
	glUseProgram(progID);
}

// Draws the fbo object to the screen
void RenderFBO::DrawFBO(FBO &fbo, int width, int height)
{
	glViewport(0, 0, width, height);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(progID);

	quad.DrawQuad(fbo.color);

	glUseProgram(0);
}
