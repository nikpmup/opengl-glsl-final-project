/*
 * Daniel Hall (dhall02)
 * CPE 471
 * Final Project
 * Render.h
 */

#ifndef _RENDERN_H
#define _RENDERN_H

#include "Mesh.h"
#include "Material.h"

class Render
{
	public:
		static int Install(char *vShader, char *fShader);	
		static void UseProg();
		static void DrawMat(CMesh &mesh, CMaterial &mat);
		static void DrawTex(CMesh &mesh, GLuint texID);
		static void DrawRimTex(CMesh &mesh, GLuint texID);
		static void DrawMatTex(CMesh &mesh, CMaterial &mat, GLuint texID);

		static void SetProjection(const mat4 &proj);
		static void SetView(const mat4 &view);
		static void SetModel(const mat4 &model);
		static void SetLight(const vec3 &pos);
		static void SetCam(const vec3 &pos);
		static void SetMat(CMaterial &mat);
		static void SetLightColor(const vec3 &col);
		static int GetProgID();
	private:
		Render();
		static void BindData();
		static void DrawMesh(CMesh &mesh);
		static void BindTex(GLuint texID);
};

#endif
