/*
 * Daniel Hall (dhall02)
 * CPE 471
 * Final Project
 * Camera.cpp
 */

#include "Camera.h"
#include <cassert>
#include <cmath>
#include "glm/gtc/matrix_transform.hpp"

#define PI 3.14159265359

CCamera::CCamera()
{
	Reset();
}

void CCamera::Reset()
{
	setProj(35.0, 1.0, 0.1, 100.0);
	pos = vec3(0, 0, 0);
	look = vec3(0, 0, -1);
	up = vec3(0, 1, 0);
	camMove = vec3(0, 0, 0);
}

// Returns the projection matrix
mat4 CCamera::getProjMatrix()
{
	if (width && height)
		ratio = (float)*width / *height;

	return glm::perspective(fov, ratio, zNear, zFar);
}

// Returns the view matrix
mat4 CCamera::getViewMatrix()
{
	//camMove.y = 0;
	return lookAt(pos + camMove + camHeight, look + camMove + camHeight, up);
}

vec3 CCamera::getPosition()
{
	return pos + camMove + camHeight;
}

// Sets the projection variables
void CCamera::setProj(CFLOAT &fov, CFLOAT &ratio, CFLOAT &zN, CFLOAT &zF)
{
	setFOV(fov);
	setRatio(ratio);
	setZNear(zN);
	setZFar(zF);
}

void CCamera::setFOV(CFLOAT &_fov)
{
	fov = _fov;
}

void CCamera::setRatio(CFLOAT &_ratio)
{
	assert(_ratio);
	width = height = NULL;
	ratio = _ratio;
}

void CCamera::setScreen(int *_width, int *_height)
{
	width = _width;
	height = _height;
}

void CCamera::setZNear(CFLOAT &z)
{
	zNear = z;
}

void CCamera::setZFar(CFLOAT &z)
{
	zFar = z;
}

void CCamera::setPos(CVEC3 &_pos)
{
	pos = _pos;
}

void CCamera::setLook(CVEC3 &_look)
{
	look = _look;
}

void CCamera::setUp(CVEC3 &_up)
{
	up = _up;
}

// Sets the view matrix parameters
void CCamera::setView(CVEC3 &pos, CVEC3 &look, CVEC3 &up)
{
	setPos(pos);
	setLook(look);
	setUp(up);
	calcCamCoord();
}

// Calculates a new look depending on the phi and theta values
void CCamera::calcLook(CFLOAT _phi, CFLOAT _theta)
{
	static float phi = 0;
	static float theta =  -PI / 2;

	const float NINETY = PI / 2;

	phi += _phi;
	theta += _theta;

	clamp(phi, 0, 5/18.0 * PI);

	look = vec3(
			cos(phi) * cos(theta), 
			sin(phi),
			cos(phi) * cos(NINETY - theta));

}

// Clamps the float between the center +/- the provided radians
void CCamera::clamp(float &angle, CFLOAT &center, CFLOAT &rad)
{
	const float rangePos = center + rad;
	const float rangeNeg = center - rad;

	if (angle > rangePos)
		angle = rangePos;
	else if (angle < rangeNeg)
		angle = rangeNeg;
}

// Calculates the camera axis vectors
void CCamera::calcCamCoord()
{	
	camZ = -normalize(look - pos);
	camX = normalize(cross(up, camZ));
	camY = normalize(cross(camX, camX)); 
}

// Moves along the negative camera x axis
void CCamera::moveForward(CFLOAT &amt)
{
	calcCamCoord();
	camMove += -camZ * amt;
}

// Moves along the positive camera x axis
void CCamera::moveBack(CFLOAT &amt)
{
	calcCamCoord();
	camMove += camZ * amt;
}

// Moves along the negative camera x axis
void CCamera::strafeLeft(CFLOAT &amt)
{
	calcCamCoord();
	camMove -= camX * amt;
}

// Move along the positive camera x axis
void CCamera::strafeRight(CFLOAT &amt)
{
	calcCamCoord();
	camMove += camX * amt;
}

// Sets the camera height on the y axis
void CCamera::setHeight(CFLOAT &height)
{
	camHeight.y = height;
}
