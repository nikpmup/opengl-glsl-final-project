/*
 * Daniel Hall (dhall02)
 * CPE 471
 * Final Project
 * RenderBlur.cpp
 */

#include "RenderBlur.h"

#include <GL/glut.h>
#include <stdio.h>
#include <cstdlib>

#include "GLSL_helper.h"
#include "InstallShader.h"
#include "Quad.h"

enum eBlur
{
	eDIR, // Direction
	eBLUR_CNT
};

static Quad quad;
static GLuint gl_blur[eBLUR_CNT];
static int progID;

// Installs the fragment and vertex shader to this render and binds data
int RenderBlur::Install(char *vShader, char *fShader)
{
	//Installs Shader
	progID = InstallShader::Install(vShader, fShader);

	// Binds GLSL Data
	glUseProgram(progID);
	RenderBlur::BindData();
	quad.BindData(progID);
	glUseProgram(0);

	return progID;
}

// Sets the GLuint to variables withing the shaders
void RenderBlur::BindData()
{
	gl_blur[eDIR] = safe_glGetUniformLocation(progID, "uDirection");
}

// Sets the program to use this render
void RenderBlur::UseProg()
{
	glUseProgram(progID);
}

// Blurs the provided fbo in a given direction
void RenderBlur::DrawBlur(FBO &fbo, int dir)
{
	if (!progID)
	{
		fprintf(stderr, "RenderBlur shader not installed yet\n");
		exit(1);
	}

	glUseProgram(progID);

	// Sets Blur Orientation
	safe_glUniform1i(gl_blur[eDIR], dir);

	// Draws Blurred FBO
	quad.DrawQuad(fbo.color);

	glUseProgram(0);
}
