/*
 * Daniel Hall (dhall02)
 * CPE 471
 * Final Project
 * Quad.cpp
 */

#include "Quad.h"
#include "GLSL_helper.h"

// Binds the GLuints to locations within the shader
void Quad::BindData(int progID)
{
	gl_fbo[VERTEX] = safe_glGetAttribLocation(progID, "aPosition");
	gl_fbo[UV] = safe_glGetAttribLocation(progID, "aUV");
	gl_fbo[TEXID] = safe_glGetUniformLocation(progID, "uTexID");

	InitQuad();
}

//  Buffers the quad data into the program
void Quad::InitQuad()
{
	static const float quad_vertex[] = 
	{
		-1.0f, -1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 0.0f
	};
	static const float  texCoord[] =
	{
		0, 0,
		0, 1,
		1, 1,
		1, 0
	};
	static const unsigned short idx[] = {0, 1, 2, 0, 2, 3 };

	glGenBuffers(1, &quadBuff[Q_POS]);
	glBindBuffer(GL_ARRAY_BUFFER, quadBuff[Q_POS]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quad_vertex), quad_vertex,
			GL_STATIC_DRAW);

	glGenBuffers(1, &quadBuff[Q_UV]);
	glBindBuffer(GL_ARRAY_BUFFER, quadBuff[Q_UV]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(texCoord), texCoord,
			GL_STATIC_DRAW);

	glGenBuffers(1, &quadBuff[Q_IDX]);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, quadBuff[Q_IDX]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(idx), idx,
			GL_STATIC_DRAW);
}

// Draws a texture onto the quad
void Quad::DrawQuad(int texture)
{
	glEnable(GL_TEXTURE_2D);
	if (texture != -1)
	{
		glActiveTexture(GL_TEXTURE0 + texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		safe_glUniform1i(gl_fbo[TEXID], texture);
	}
	
	safe_glEnableVertexAttribArray(gl_fbo[VERTEX]);
	glBindBuffer(GL_ARRAY_BUFFER, quadBuff[Q_POS]);
	safe_glVertexAttribPointer(gl_fbo[VERTEX], 3, GL_FLOAT,
			GL_FALSE, 0, 0);

	safe_glEnableVertexAttribArray(gl_fbo[UV]);
	glBindBuffer(GL_ARRAY_BUFFER, quadBuff[Q_UV]);
	safe_glVertexAttribPointer(gl_fbo[UV], 2, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, quadBuff[Q_IDX]);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);

	safe_glDisableVertexAttribArray(gl_fbo[UV]);
	safe_glDisableVertexAttribArray(gl_fbo[VERTEX]);

	glDisable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);
}
