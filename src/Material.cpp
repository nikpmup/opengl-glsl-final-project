/*
 * Daniel Hall (dhall02)
 * CPE 471
 * Final Program
 * Material.cpp
 */

#include "Material.h"
#include <cassert>

CMaterial::CMaterial() {}

CMaterial::CMaterial(CVEC3 &amb, CVEC3 &diff, CVEC3 &spec, CFLOAT &refl)
{
	setAmb(amb);
	setDiff(diff);
	setSpec(spec);
	setRefl(refl);
}

void CMaterial::setAmb(CVEC3 &amb)
{
	ambCoef = amb;
}

void CMaterial::setDiff(CVEC3 &diff)
{
	diffCoef = diff;
}

void CMaterial::setSpec(CVEC3 &spec)
{
	specCoef = spec;	
}

void CMaterial::setRefl(CFLOAT &refl)
{
	reflect = refl;
}

CVEC3& CMaterial::getAmb()
{
	return ambCoef;
}

CVEC3& CMaterial::getDiff()
{
	return diffCoef;
}

CVEC3& CMaterial::getSpec()
{
	return specCoef;
}

CFLOAT& CMaterial::getRefl()
{
	return reflect;
}
