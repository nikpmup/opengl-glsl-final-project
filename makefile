CC = g++
GL = -DGL_GLEXT_PROTOTYPES -lGL -lGLU -lglut -O3 -Wall
SRC_DIR = src
OBJ_DIR = obj
SRC = $(wildcard $(SRC_DIR)/*.cpp)
OBJ = $(patsubst $(SRC_DIR)/%.cpp, $(OBJ_DIR)/%.o, $(SRC))
EXEC = a.out


all : $(OBJ_DIR) $(EXEC) $(OBJ)

$(EXEC) : $(OBJ)
	$(CC) -o $@ $^ $(GL)

$(OBJ_DIR) : 
	mkdir $@

$(OBJ_DIR)/%.o : $(SRC_DIR)/%.cpp
	g++ -c -o $@ $< $(GL)

clean :
	rm -rf obj $(EXEC)
