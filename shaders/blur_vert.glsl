// Data
attribute vec3 aPosition;
attribute vec2 aUV;

uniform int uDirection;

// Fragment stuff
varying vec2 vUV;
varying vec2 v_blurTexCoords[14];

void main()
{
	gl_Position = vec4(aPosition, 1.0f);
	vUV = aUV;
	if (uDirection == 0)
	{
		v_blurTexCoords[ 0] = aUV + vec2(-0.028, 0.0);
		v_blurTexCoords[ 1] = aUV + vec2(-0.024, 0.0);
		v_blurTexCoords[ 2] = aUV + vec2(-0.020, 0.0);
		v_blurTexCoords[ 3] = aUV + vec2(-0.016, 0.0);
		v_blurTexCoords[ 4] = aUV + vec2(-0.012, 0.0);
		v_blurTexCoords[ 5] = aUV + vec2(-0.008, 0.0);
		v_blurTexCoords[ 6] = aUV + vec2(-0.004, 0.0);
		v_blurTexCoords[ 7] = aUV + vec2( 0.004, 0.0);
		v_blurTexCoords[ 8] = aUV + vec2( 0.008, 0.0);
		v_blurTexCoords[ 9] = aUV + vec2( 0.012, 0.0);
		v_blurTexCoords[10] = aUV + vec2( 0.016, 0.0);
		v_blurTexCoords[11] = aUV + vec2( 0.020, 0.0);
		v_blurTexCoords[12] = aUV + vec2( 0.024, 0.0);
		v_blurTexCoords[13] = aUV + vec2( 0.028, 0.0);
	}
	else 
	{
		v_blurTexCoords[ 0] = aUV + vec2(0.0, -0.028);
		v_blurTexCoords[ 1] = aUV + vec2(0.0, -0.024);
		v_blurTexCoords[ 2] = aUV + vec2(0.0, -0.020);
		v_blurTexCoords[ 3] = aUV + vec2(0.0, -0.016);
		v_blurTexCoords[ 4] = aUV + vec2(0.0, -0.012);
		v_blurTexCoords[ 5] = aUV + vec2(0.0, -0.008);
		v_blurTexCoords[ 6] = aUV + vec2(0.0, -0.004);
		v_blurTexCoords[ 7] = aUV + vec2(0.0,  0.004);
		v_blurTexCoords[ 8] = aUV + vec2(0.0,  0.008);
		v_blurTexCoords[ 9] = aUV + vec2(0.0,  0.012);
		v_blurTexCoords[10] = aUV + vec2(0.0,  0.016);
		v_blurTexCoords[11] = aUV + vec2(0.0,  0.020);
		v_blurTexCoords[12] = aUV + vec2(0.0,  0.024);
		v_blurTexCoords[13] = aUV + vec2(0.0,  0.028);
	}
}
