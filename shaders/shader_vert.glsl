// Data
attribute vec3 aPosition;
attribute vec3 aNormal;
attribute vec2 aTexCoord;

// Transform stuff
uniform mat4 uProjMatrix;
uniform mat4 uViewMatrix;
uniform mat4 uModelMatrix;
uniform mat4 uNormMatrix;

// Positions
uniform vec3 uLightPos;
uniform vec3 uCamPos;

// Fragment Stuff
varying vec3 xNorm;
varying vec3 xLight;
varying vec3 xCamera;
varying vec3 xCamPos;
varying vec2 vTexCoord;

void main() 
{
   // Calculate Position
   vec4 wPosition = uModelMatrix * vec4(aPosition, 1);
   vec4 pPosition = uViewMatrix * wPosition;
	vec4 fPosition = uProjMatrix * pPosition;
   gl_Position = fPosition;

   // Camera
   vec3 nCam = -vec3(pPosition);

   //Calculate Normal
   vec4 vNorm = vec4(normalize(aNormal), 0);
   vNorm = uNormMatrix * vNorm;
   vec3 nNorm = normalize(vec3(vNorm));

   //Calculate Light
   vec3 nLight = normalize(uLightPos - vec3(wPosition));

   xNorm = nNorm; 
   xLight = nLight;
   xCamPos = uCamPos - wPosition.xyz;
	xCamera = nCam;
   vTexCoord = aTexCoord;
}
