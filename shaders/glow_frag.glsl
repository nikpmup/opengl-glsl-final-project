uniform sampler2D uSrc;
uniform sampler2D uDst;

varying vec2 vUV;

void main()
{
	vec4 srcColor = texture2D(uSrc, vUV);
	vec4 dstColor = texture2D(uDst, vUV) / 2.0;

	// Additive
	//gl_FragColor = min(srcColor + dstColor, 1.0);
	gl_FragColor = clamp((srcColor + dstColor) - (srcColor * dstColor), 0.0, 1.0);
	gl_FragColor.w = 1.0f;
}
