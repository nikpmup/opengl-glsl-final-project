// Color
uniform vec3 uLightColor;
uniform vec3 uAmbColor;
uniform vec3 uDiffColor;
uniform vec3 uSpecColor;
uniform float uRefl;

// Flags
uniform int uTexFlag;
uniform int uComb;
uniform int uRim;

// Texture
uniform sampler2D uTexID;

// Fragment
varying vec3 xNorm;
varying vec3 xLight;
varying vec3 xCamera;
varying vec3 xCamPos;
varying vec4 xColor;
varying vec2 vTexCoord;

vec3 rimLight(vec3 norm, vec3 cam, vec3 color, 
		float start, float end, float multiplier);

void main()
{
	vec3 nNorm = normalize(xNorm); 
	vec3 nLight = normalize(xLight);
	vec3 nCamera = normalize(xCamera);
	vec3 nCamPos = normalize(xCamPos);
	vec4 src;
	vec3 diffC;
	
	if(uTexFlag == 1)
	{
		src = texture2D(uTexID, vTexCoord);
		if (uRim == 1)
			src.rgb += rimLight(nNorm, nCamPos, vec3(1.0f), 0.5f, 2.0f, 2.0f);

		gl_FragColor = src;
	}

	if (uTexFlag == 0 || uComb == 1)
	{
		float diffI = max(0.0, dot(nNorm, nLight));
		vec3 refl = -nLight + 2.0 * max(0.0, dot(nNorm, nLight)) * nNorm;
		float specI = pow(max(0.0, dot(nCamera, normalize(refl))), uRefl);

		diffC = (uComb == 1 ? src.rgb : uDiffColor) * diffI * uLightColor;

		gl_FragColor = vec4(diffC  + uAmbColor, 1.0);
	}
}

vec3 rimLight(vec3 norm, vec3 cam, vec3 color, 
		float start, float end, float multiplier)
{
	float normToCam = max(0.0, dot(norm, cam));	
	float rim = smoothstep(start, end, normToCam);

	return color * rim * multiplier;
}
