// Data
attribute vec3 aPosition;
attribute vec2 aUV;

// Fragment stuff
varying vec2 vUV;
varying vec2 v_blurTexCoords[14];

void main()
{
	gl_Position = vec4(aPosition, 1.0f);
	vUV = aUV;
}
