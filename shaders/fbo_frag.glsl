// Texture 
uniform sampler2D uTexID;

// Fragment
varying vec2 vUV;

void main()
{
	vec4 color = texture2D(uTexID, vUV);
	if (color.w > 0.2)
		gl_FragColor = color;
	else
		discard;
}
