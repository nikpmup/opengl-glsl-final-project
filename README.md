Daniel Hall (dhall02)
Program 4
CPE 471

makefile
------------------------------------------------------------------------------
make - 	creates an executable called a.out. Searches for .cpp files in the src
	folder and compiles them to .o files in the obj folder. executable
	will be created in root project folder.

make clean - removes executables and object files


Notes
------------------------------------------------------------------------------
I designed my program throug the stages of adding a bloom effect. The initial
start up the program will seem like nothing is done. Please read the Keybinds
to see how you can progress through the steps or skip to my final render scene
. Navi can move as well with the proper keybind press! Read the keybinds!!!

How To Use
------------------------------------------------------------------------------
Keybinds
	
	'a' - strafe left
	'd' - strafe right
	'w' - move forward
	'd' - move back

	!!!!/* READ THIS PART PLOX */!!!!!!
	'p' - progress through render steps (allows you to see the steps of
		"Bloom")
	'f' - skips to the final render scene
	'r' - resets to the basic Render
	'm' - moves Navi!

Mouse
	look around, no button clicks required


Design Choices
------------------------------------------------------------------------------
For this project I wanted to seperate all functionality into seperate classes
and files. This begins with the

Mesh 	    simple class that can load a mesh/obj file. Functionality include
	    centering the mesh around a point and resizing it. Lastly, it
	    will calculate the normals of the mesh. Contains a CTM which
	    handles the modelview matrix

Material    Class containing information such as ambient light values,
	    diffuse color, specular colors, and reflection amount

Camera 	    The camera object contained information such as the project and 
	    view of the camera. It handled information such as the Camera
	    space and which vectors were forward, back, up, and down

Render 	    Renders a mesh to the screen. Various options exist if the user
	    wants to draw the mesh with material, texture, both, or texture
	    with a rim lighting effect.

RenderFBO   Render that draws an FBO texture onto the screen by mapping it to
	    a quad.

RenderBlur  Render that draws an FBO with a blur effect. It can handle both
	    horizontal and vertical directions.

RenderGlow  Render that blends two fbos with screen blending

Texture     Loads a texture into the program with the ability to choose
	    between clamp or repeat mode.

Main 	    Main had all the information on which objects were being created 
	    in the scene and called the render to draw them. Additionally, it
	    handled the IO and tied IO functions to camera movement. the
	    design decision I had with the mouse was to remove the click and 
	    move. This was accomplished by using a passive motion function and
	    to create a smooth movement, the mouse cursor was warped to the
	    center of the screen whenever it got within 50 pixes from the edge
	    of the screen. However if you move your mouse fast enough you can
	    get out of the window so the program is started in fullscreen to
	    prevent this.
